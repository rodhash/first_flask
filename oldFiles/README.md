# First_flask

## First Flask app
- This Web Page was built using Flask, to run a local server via python (on Linux):

    **shell:**
```sh
    export FLASK_APP=server.py
    flask run
```

## Home Preview

![](1st_Flask.png)

