
Flask
=======

    Some generic notes about the content just learned.

    Usually I keep them in my personal git but for the purpose of this class I'm keeping those few notes as public.


## - Creating the venv
- python3 -m venv .
```python
    vim-subshell:  PC flask ⎇  master ⟲  ➜  lt
    total 12
    -rw-rw-r--. 1 rodhash rodhash   0 Apr 10 13:08 flask.md
    -rw-rw-r--. 1 rodhash rodhash  37 Apr 10 13:13 style.css
    -rw-rw-r--. 1 rodhash rodhash  21 Apr 10 13:17 script.js
    -rw-rw-r--. 1 rodhash rodhash 392 Apr 10 13:37 index.html
    vim-subshell:  PC flask ⎇  master ⟲  ➜
    vim-subshell:  PC flask ⎇  master ⟲  ➜  python3 -m venv .
    vim-subshell:  PC flask ⎇  master ⟲  ➜  lt
    total 28
    -rw-rw-r--. 1 rodhash rodhash    0 Apr 10 13:08 flask.md
    -rw-rw-r--. 1 rodhash rodhash   37 Apr 10 13:13 style.css
    -rw-rw-r--. 1 rodhash rodhash   21 Apr 10 13:17 script.js
    -rw-rw-r--. 1 rodhash rodhash  392 Apr 10 13:37 index.html
    drwxrwxr-x. 2 rodhash rodhash 4096 Apr 10 14:03 include
    -rw-rw-r--. 1 rodhash rodhash   69 Apr 10 14:03 pyvenv.cfg
    lrwxrwxrwx. 1 rodhash rodhash    3 Apr 10 14:03 lib64 -> lib
    drwxrwxr-x. 3 rodhash rodhash 4096 Apr 10 14:03 lib
    drwxrwxr-x. 2 rodhash rodhash 4096 Apr 10 14:03 bin
    vim-subshell:  PC flask ⎇  master ⟲  ➜
    ```

## - Activating the venv
- . venv/bin/activate

    vim-subshell:  PC flask ⎇  master ⟲  ➜  . bin/activate
    (flask) vim-subshell:  PC flask ⎇  master ⟲  ➜


## - Install flask
- pip3 install flask --user


## - Running flask
- export FLASK_APP=server.py
- export FLASK_ENV=development
- flask run
```
    vim-subshell:  PC flask ⎇  master ⟲  ➜  . bin/activate
    (flask) vim-subshell:  PC flask ⎇  master ⟲  ➜  export FLASK_APP=server.py
    (flask) vim-subshell:  PC flask ⎇  master ⟲  ➜  flask run server.py
    Usage: flask run [OPTIONS]
    Try "flask run --help" for help.

    Error: Got unexpected extra argument (server.py)
    (flask) vim-subshell:  PC flask ⎇  master ⟲  ➜  flask run
     * Serving Flask app "server.py"
     * Environment: production
       WARNING: This is a development server. Do not use it in a production deployment.
       Use a production WSGI server instead.
     * Debug mode: off

    0.00093
     * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
    127.0.0.1 - - [10/Apr/2020 14:15:37] "GET / HTTP/1.1" 200 -
    127.0.0.1 - - [10/Apr/2020 14:15:37] "GET /favicon.ico HTTP/1.1" 404 -
```


