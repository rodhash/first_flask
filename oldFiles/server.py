#!/usr/bin/env python3

# IMPORTING 'timing'
import sys
sys.path.insert(1, '/home/rodhash/git-rodhash/Python/myModules/')
import timing

import os
from flask import Flask, render_template, send_from_directory, url_for
app = Flask(__name__)


# @app.route('/<username>/<int:post_id>')
@app.route('/')
def my_home():
    # return 'Hello World!!!!!'
    print(url_for('static', filename='favicon.png'))
    return render_template('index.html')

@app.route('/<string:page_name>')
def html_page(page_name):
    return render_template(page_name)


# @app.route('/index.html')
# def my_home2():
#     return render_template('index.html')
#
# @app.route('/works.html')
# def my_works():
#     return render_template('works.html')
#
# @app.route('/about.html')
# def about():
#     return render_template('about.html')
#
# @app.route('/contact.html')
# def contact():
#     return render_template('contact.html')
#
# @app.route('/components.html')
# def comp():
#     return render_template('components.html')



# @app.route('/blog')
# def blog():
#     return 'This is my blog!'
#
# @app.route('/blog/2020/dogs')
# def blog2():
#     return 'This is my dog!'
#
# @app.route('/about.html')
# def about():
#     return render_template('about.html')
#
# @app.route('/favicon.ico')
# def favicon():
#     # return send_from_directory('flask-icon.png')
#     return send_from_directory(os.path.join(app.root_path, 'static'),
#                                 'favicon.png', mimetype='image/favicon.png')
#

timing.timing()
